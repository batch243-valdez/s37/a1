

// Setup dependencies
require('dotenv').config()
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes")
const app = express();
app.use(cors());
app.use(express());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// 

mongoose.connect(process.env.MONGO_URI,
        {
            useNewUrlParser:true,
            useUnifiedTopology: true
        });

mongoose.connection.on("error", console.error.bind(console, "connection error"));
mongoose.connection.once("open", () => console.log("You are connected to your Database"));


app.use("/users", userRoutes);
app.use("/courses", courseRoutes );

// This syntax will allow flexibility when using the application both locally or as a hosted app
// process.env.PORT is that can be assigned by your hosting service
app.listen(process.env.PORT, () => {
    console.log(`API is now online on port ${process.env.PORT}`);
})