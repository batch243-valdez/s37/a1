const User = require('../models/User');
const Course = require('../models/Course')
const bcrypt = require('bcrypt');
const mongoose = require('mongoose')
const auth = require("../auth");





const checkEmailExists = async (req, res, next) => {
    const user = req.body.email
    const emailResult = await User.find({email: user})
    if (!emailResult){
        next();
    }
    return res.status(404).json({mssg: `That email: ${user} is already taken.`});
}


const registerUser = async (req, res) => {
    
    let newUser = await new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNo: req.body.mobileNo
    })

    return newUser.save().then(user => {
        console.log(user);
        res.send(`Congratulations, Sir/mam ${newUser.firstName}!. You are now registered!`)
    }).catch(error => {
        console.log(error);
        res.send(`Sorry ${newUser.firstName}, there was an error during the registration`)
    })
}



const loginUser = async (req, res, next) => {

        const loginCreds = await User.findOne({email : req.body.email})
        if(!loginCreds){
            return res.status(404).json(`Your email: ${req.body.email} is not yet registered. Registered first!`)
        }
        else{
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, loginCreds.password);
            if(isPasswordCorrect){
                res.status(200).json({accessToken: auth.createAccessToken(loginCreds)});
            }
            else{
                res.status(404).json(`Password is incorrect!`)
            }
        }
}


const getProfile = async ( req, res ) => {
    const { id } = req.body


    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(404).json(`No user registered by the ID you provided!`)
    }

    const user = await User.findById(id)

    if(user){
        user.password = "";
        return res.status(200).json(user);
    }

    
}


const profileDetails = (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    console.log(userData);

    return User.findById(userData.id)
    .then(result => {
        result.password = "Confidential"
        return res.send(result)
    })
    .catch(err => {
        return res.send(err);
    })
}


const updateRole = (req, res) => {
    const token =  req.headers.authorization
    const userData =  auth.decode(token);

    const idTobeUpdated = req.params.userId;

    if(userData.isAdmin){
        return User.findById(idTobeUpdated).then(result => {
            let update = {
                isAdmin: !result.isAdmin
            }
            return User.findByIdAndUpdate(idTobeUpdated, update, {new: true})
            .then(document => res.send(document))
            .catch(err => res.send(err));
        })
    }
    else{
        return res.send("You dont have access on this page!")
    }
}


const enroll = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decode(token);

    if(!userData.isAdmin){

        let courseId = req.params.courseId;

        let data = {
            courseId : courseId,
            userId : userData.id
        }

        let isCourseUpdated = await Course.findById(data.courseId)
        .then(result => {
            result.enrollees.push({
                userId: data.userId
            })
            result.slots -= 1;
            return result.save()
            .then(success => {return true;})
            .catch(err => {return false;})
        })
        .catch(err => { return res.send(false)})

        let isUserUpdated = await User.findById(data.userId)
        .then(result => {
            result.enrollments.push({
                courseId: data.courseId
            })
            return result.save()
            .then(success => {return true;})
            .catch(err => {return false;})
        })
        .catch(err => { return res.send(false)})

        return (isUserUpdated && isCourseUpdated) ? res.send("You are now enrolled!") : res.send("We encountered an error in your enrollment, please try again!")
    }
    else{
        return res.send("Admin is not allowed to enroll a course")
    }
}


module.exports = {
    checkEmailExists,
    registerUser,
    loginUser,
    getProfile,
    profileDetails,
    updateRole,
    enroll
}