
const Course = require('../models/Course')
const auth = require("../auth");
const { response } = require('express');



const addCourse = async (req, res) => {
    
    try{
    const user = auth.decode(req.headers.authorization)
    
        if(user.isAdmin){
            const newCourse =  await new Course({
                name: req.body.name,
                description: req.body.description,
                price: req.body.price,
                slots: req.body.slots
            })

            newCourse.save()
                .then(result => {
                    console.log(result)
                    res.send(true)
                })
                .catch(err => {
                    console.log(err);
                    res.send(true);
                })
        }
        else{
        res.status(404).json("Only admin have permission to add course!")
        }
    }catch(err){
        res.status(404).json({error: err.message})
    }  
}

// Retrieve all active courses

const getAllActive = async (req, res) => {
    return Course.find({isActive: true})
    .then(result =>{
        res.send(result);
    })
    .catch(err =>{
        res.send(err);
    })
}

const getCourse = async ( req, res ) => {
    const courseId = req.params.courseId

    return await Course.findById(courseId)
    .then(result => {
        res.send(result);
    })
    .catch(err => {
        res.send(err);
    })
}

const updateCourse = async(req, res) => {
    const token = req.headers.authorization
    const userData = auth.decode(token);
    console.log(userData)
    
        let updatedCourse = {
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            slots: req.body.slots
        }

    const courseId = req.params.courseId;

    if(userData.isAdmin){
        return Course.findByIdAndUpdate(courseId, updatedCourse, {new:true})
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.send(err);
        })
    }else{
        return res.send("You don't have acces to this page!")
    }
}

const archiveCourse = async (req, res) => {
    
    try{
        const userData = auth.decode(req.headers.authorization)
        if(userData.isAdmin){
            const { courseId } = req.params

            const archivedCourse = await Course.findOneAndUpdate({_id: courseId}, {
                ...req.body
            })
            console.log(archivedCourse);
            if(!archivedCourse){
                return res.status(404).json({error: `No such Course`})
            }
            res.status(200).json(true)
        }else{
        res.status(404).json("Only admin have permission to add course!")
        }
    }catch(err){
        res.status(404).json({error: err.message})
    }        
}


const getAllCourses = async(req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decode(token)
    
    if(!userData.isAdmin){
       return res.status(404).json("Sorry, you dont have access to this page!")
    }else{
        const allCourses = await Course.find({})
        if(allCourses){
            res.status(200).json(allCourses)
        }
        res.status(404).json({error: err.message})
    }
}





module.exports = {
    addCourse,
    getAllActive,
    getCourse,
    updateCourse,
    archiveCourse,
    getAllCourses
}