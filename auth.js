const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI"

// [SECTION] JSON Web token

// Token creation
/*
    Analogy:
        Pack the gift provided with a lock, which can only be opened using the secret code as the key
*/

const createAccessToken = (loginCreds) => {
    // payload of the JWT
    const data = {
        id: loginCreds._id,
        email: loginCreds.email,
        isAdmin: loginCreds.isAdmin
    }

    // Generate a JSON web token using the jwt's sign method.
        // Syntax:
            // jtw.sign(payload, secretOrPrivateKey, [callbackfunction])

    return jwt.sign(data, secret, {})
}

// Token verification

/*
    -Analogy
        receive the gift and open the lock to verify if the sender is legitimate and the gift was not tamper with
*/

const verify = (req, res, next) => {

    let token = req.headers.authorization;

    console.log(token);

    if(token !== undefined){

        // Validate the "token" using verify method, to decrypt the token using the secret code
        /*
            Syntax:
            jwt.verify?(token, secret, [callback function])
        */

        token = token.split(" ")[1];

        return jwt.verify(token, secret, (error) => {
            if(error){
                res.status(404).json("Invalid Token");
            }
            else{
                next();
            }
        })
    }
    else{
        res.status(404).json("Authentication failed! No Token provided")
    }
}

// Token decryption
/*
    -Analogy
        Opening gift or unwrapping presents
 */

const decode = (token) => {
    if(token === undefined){
        return null
    }
    else{
        token = token.slice(7, token.length)

        return jwt.verify(token, secret, (error, data) => {
            if(error){
                return null;
            }else{
                // decode method is used to obtain the information from the JWT
                // Syntax: jwt.decode(token, [options])
                // Return an object with the access to the payload property.
                return jwt.decode(token, {complete: true}).payload
            }
        })
    }
}


module.exports = {
    createAccessToken,
    verify,
    decode
}
