const mongoose = require("mongoose")
const Schema = mongoose.Schema;
const courseSchema = new Schema({
    name: {
        type: String,
        required: [ true, "Course is required" ]
    },
    description:{
        type: String,
        required: [ true, "Description is required" ]
    },
    price:{
        type: String,
        required: [ true, "Price is required"]
    },
    slots: {
        type: Number,
        required: [true, "slot is required"]
    },
    isActive:{
        type: Boolean,
        default: true
    },
    createdOn:{
        type: Date,
        default: new Date()
    },
    enrollees:[
        {
            userId:{
                type: String,
                required: [true, "UserId is required"]
            },
            enrolledOn:{
                type: Date,
                default: new Date()
            }
        }
    ]
});

module.exports = mongoose.model("Course", courseSchema)

// Activity

// Create a "User.js" file to store the schema of our users
// models > user.js